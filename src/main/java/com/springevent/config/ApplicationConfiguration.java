package com.springevent.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.springevent.controller.Circle;

@Configuration
@ComponentScan(basePackages = "com.springevent.services")
public class ApplicationConfiguration {
     
    @Bean
    public Circle circle() {
        return new Circle();
    }
     
}