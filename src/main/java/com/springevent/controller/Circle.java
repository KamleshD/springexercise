package com.springevent.controller;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Controller;

import com.springevent.events.DrawEvent;

@Controller
public class Circle implements ApplicationEventPublisherAware {
	private ApplicationEventPublisher publisher;

		public void draw() {
		System.out.println("Circle is Drawn");
		DrawEvent drawEvent = new DrawEvent(this);
		publisher.publishEvent(drawEvent);
	}

	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}
}