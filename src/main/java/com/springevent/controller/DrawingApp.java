package com.springevent.controller;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.springevent.config.ApplicationConfiguration;

public class DrawingApp {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(ApplicationConfiguration.class);
		context.refresh();
		Circle circle = (Circle) context.getBean("circle");
		circle.draw();
	}
}